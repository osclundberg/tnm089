function [ output ] = getWindow( i,j, image, intensityI, intensityJ, variances, means )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    a = [];
    wk = 3*3; % number of pixels in window
    epsilon = 0.0001;

    uk = means(j,i);
    variance = variances(j,i);
    
    if i == j
        kronecker = 1;
    else
        kronecker = 0;
    end
    
    
    % Weight function output
    
    sum = kronecker - (1/wk)*( 1+ (1/( (epsilon/wk) + variance )) ...
        *(intensityI - uk) * (intensityJ - uk));
    
    if isnan(sum)
        output = 0;
    else
        output = sum;
    end
    
    
end

