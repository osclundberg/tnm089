clc;
clear;

%no white border around imshow
iptsetpref('ImshowBorder','tight'); 

img = double(imread('img/teddy_ear.bmp'))/255;

%img = img(:, :, 1);
img = rgb2gray(img);


[height width channels] = size(img);
N = width * height;

I = reshape(img, N, 1);

means = zeros(width,height);
variances = zeros(width,height);

img = padarray(img, [1 1]);

L = zeros(N);

for i = 1:height-2
    for j = 1:width-2
        a = [];
        a = [a; img(i:3,j)];
        a = [a; img(i:3,j+1)];
        a = [a; img(i:3,j+2)];
        
        variances(i,j) = var(a);
        means(i,j) = mean(a);
    end
end

random = 12

%%

h = waitbar(0, 'Processing..');
for i = 1:N
    for j = 1:N
        % Extract corresponding x,y values in image
        if mod(j, width) == 0
            x = ceil(j/width);
        else
            x =  ceil(j/width); 
        end
        
        if mod(j, width) == 0
            y = width;
            
        else
            y = mod(j, width);
            
        end 
        
        L(i,j) = getWindow(x, y, img, I(i), I(j), variances, means);
      
    end
    perc = i/N; 
    waitbar(perc, h, sprintf('... %.2f',perc));

end

close(h);

%%


eigenvector = eig(L);


% img = reshape(img, width, height);


%%

for i = 1:width
    for j = 1:height
    
        if alpha_img(i,j) < 0
           alpha_img(i,j) = 1;
        end
        
    end
end

