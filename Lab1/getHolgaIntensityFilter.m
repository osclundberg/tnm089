function [ filtered_im ] = getHolgaIntensityFilter( original_im )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
%%
% Transform to polar coordinates
x_max = size(original_im, 2) / 2;
y_max = size(original_im, 1) / 2;

[X, Y] = meshgrid(-x_max:x_max - 1, -y_max:y_max - 1);
[TH, R] = cart2pol(X, Y); %#ok<ASGLU>

%%
mean_im = (original_im(:, :, 1) + original_im(:, :, 2) + original_im(:, :, 3)) / 3;

% Put radii and intensity in vectors
B = reshape(mean_im, [], 1);

r = reshape(R, [], 1);
r2 = r.^2;
r0 = ones(size(r,1), 1);
A = cat(1, r2', r', r0')';

% Get coefficients
k = A \ B

% Create filter
filtered_im = reshape(A * k, size(original_im(:, :, 1)));
filtered_im = filtered_im / max(filtered_im(:));

end

