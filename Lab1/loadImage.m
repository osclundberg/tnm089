function [ s ] = loadImage( file_path )
%UNTITLED4 Loads an image in the memory
%   
[im_raw, im_info] = getRawImage(file_path);

%%
% Selecting channels and casting to double
im_r = double(im_raw(1:2:end,1:2:end));
im_g = double(im_raw(2:2:end,1:2:end));
im_b = double(im_raw(2:2:end,2:2:end));


%%
% Compose the image and return a struct

s = struct;
s.image = cat(3, im_r, im_g, im_b);
s.info = im_info;
end

