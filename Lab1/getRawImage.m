function [theRawImage,ifexif] = getRawImage( theTotalFileName)
%getRawImage read rawimage using dcraw
%Usage: theRawImage = getRawImage( theTotalFileName);
%Optional input: theTotalFileName
%Description: loads raw image

%% initialize
persistent dcrawinit dcrawstring tmppath

if isempty(dcrawinit) 
    dcrawinit = 'done';
    DCPath = fullfile('H:','Kurser','TNM089','Lab1');
    dcrawstring = [DCPath,'\dcraw64.exe  -4 -D -T -c '];
    tmppath = 'h:\tmpimage.tiff';
end

%% Actual Filename to use
if exist( 'theTotalFileName','var')
    myTotalFileName = theTotalFileName;
else
    %Bildauswahl:
    [myTotalFileName, myStatus] = getRawFile4Read( '*.*');
    if myStatus==0
        return;
    end
end

%%

system( [dcrawstring, myTotalFileName,' > ',tmppath]);

theRawImage = imread(tmppath);
if nargout > 1
    s = warning;
    try
        ifexif = imfinfo (tmppath);
    catch
        ifexif = [];
    end
    warning(s);
end
delete(tmppath);

end

function [ theTotalFileName, theStatus] = getRawFile4Read( theFileExtension)
% usage:  [ theTotalFileName, theStatus] = getRawFile4Read( theFileExtension);
% hint:   theTotalFileName is valid if theStatus==1 

if exist( 'theFileExtension','var')
    myFileFilter = theFileExtension;
else
    myFileFilter = '*.*';
end

WorkingDirectory = pwd();

[Bilddateiname, Pfad] = uigetfile( myFileFilter, 'Choose Raw Image:');
if( exist( 'Bilddateiname')==0 || ischar( Bilddateiname)==0)
    myStatus = 0;
else
    myStatus = 1;
end
if strcmp( WorkingDirectory, Pfad)==1
    UsePfad = ''; 
else
    UsePfad = Pfad;
end
theTotalFileName = [ UsePfad, Bilddateiname];

if nargout > 1
    theStatus = myStatus;
end

return 
end


