%%
%%%%% Lab 1 %%%%% 
% Part 1 - Intensity filter

% TODO: Refactor with s = struct...
clc;
clear;
close all;

%%
% Reading the raw files
holga_white = loadImage('H:\Kurser\TNM089\lab1\img\HWhite_9542.CR2');
canon_white = loadImage('H:\Kurser\TNM089\lab1\img\CWhite_9541.CR2');

%%
% Normalization
holga_white.image = holga_white.image / max(holga_white.image(:));
canon_white.image = canon_white.image / max(canon_white.image(:));

% Display images
%imshow(holga_white.image, []);
%figure
%imshow(canon_white.image, []);

%%
% Apply filter
filter_im = getHolgaIntensityFilter(holga_white.image);

filtered_canon_R = filter_im .* canon_white.image(:, :, 1);
filtered_canon_G = filter_im .* canon_white.image(:, :, 2);
filtered_canon_B = filter_im .* canon_white.image(:, :, 3);

filtered_canon = cat(3, filtered_canon_R, filtered_canon_G, filtered_canon_B);

imshow(filter_im)
figure;

imshow(filtered_canon);

%%
% Part 2 - Geometric calibration

holga_checker = loadImage('H:\Kurser\TNM089\lab1\img\HChecker_9537.CR2');
canon_checker = loadImage('H:\Kurser\TNM089\lab1\img\CChecker_9538.CR2');

%%
% Normalization
holga_checker.image = holga_checker.image / max(holga_checker.image(:));
canon_checker.image = canon_checker.image / max(canon_checker.image(:));

%%
% Geometric calibration 
%canon_checker.samples = [156 158; 274 158; 276 277; 158 278];
%holga_checker.samples = [62 166; 187 166; 191 296; 64 298];
%{
holga_checker.samples = [60 168; ...
323 163; ...
619 161; ...
916 157; ...
1201 152; ...
1506 147; ... 
60 300; ... % 7
61 573; ...
65 702; ...
67 966; ...
66 1105; ... % 11
205 1241; ...
626 1234; ...
1218 1227; ];
%}

%{
canon_checker.samples = [157 159; ...
394 155; ...
649 151; ...
916 148; ...
1173 143; ...
1441 140; ...
158 278; ... %7
164 516; ...
163 635; ...
166 871; ...
167 991; ... % 11
287 1110; ...
664 1105; ...
1186 1100;];

%}

holga_checker.samples = [191 295; 194 168; 61 168; 63 297]; % adjacent data points
canon_checker.samples = [276 276; 274 157; 155 158; 159 277]; % adjacent data points


H = cat(2, holga_checker.samples(:,1), holga_checker.samples(:,2),ones( size(holga_checker.samples, 1), 1));
C = cat(2, canon_checker.samples(:,1), canon_checker.samples(:,2), ones( size(canon_checker.samples, 1), 1));

A = (C \ H);

A(1, 3) = 0;
A(2, 3) = 0;
A(3, 3) = 1;

T = maketform('affine', A);
transformed_image = imtransform(canon_checker.image, T, ...
'XData', [1 size(canon_checker.image, 2)], ...
'YData', [1 size(canon_checker.image,1)]);

imshow(transformed_image);

figure;

imshow(holga_checker.image);